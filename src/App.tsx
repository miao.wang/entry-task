import "./App.css";
import { Layout } from "antd";
import { Routes, Route, Navigate } from "react-router-dom";
import routes from "./config";
import styled from "styled-components";
import { Suspense } from "react";
import SideMenu from "./components/SideMenu";

const { Sider, Content } = Layout;

const App = () => (
  <div className="App">
    <StyledLayout>
      <Layout className="layout">
        <Sider>
          <SideMenu />
        </Sider>
        <Content>
          <Suspense fallback={<div>waiting</div>}>
            <Routes>
              {routes.map((route) => (
                <Route
                  path={route.path}
                  element={<route.main />}
                  key={route.menuName}
                />
              ))}
              <Route path="*" element={<Navigate to="/create" />} />
            </Routes>
          </Suspense>
        </Content>
      </Layout>
    </StyledLayout>
  </div>
);

export default App;

const StyledLayout = styled.div`
  min-height: 100vh;
  .layout {
    height: 100vh;
  }
  .icon {
    height: 32px;
    margin: 16px;
    background-color: rgba(255, 255, 255, 0.2);
  }
`;
