import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

export const useRouteMenu = () => {
  const [selectedKeys, setSelectKeys] = useState<string[]>([]);
  const navigate = useNavigate();

  const chooseMenu = ({ key }: { key: string }) => navigate(key);

  useEffect(() => {
    setSelectKeys([window.location.pathname]);
  }, [window.location.pathname]);

  return { selectedKeys, chooseMenu };
};
