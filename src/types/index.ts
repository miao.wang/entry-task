export interface TaskData {
  content: string;
  deadline: number;
  priority: number;
  status: number;
}
export interface FilterInfo {
  content: string[];
  priority: number[];
  before: number[];
  status: number[];
}
