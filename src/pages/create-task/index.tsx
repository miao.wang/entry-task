import { Card } from "antd";
import { FC } from "react";
import styled from "styled-components";
import CreateForm from "../../components/CreateForm";

interface Props {}

const CreateTask: FC<Props> = () => {
  return (
    <StyledCard>
      <Card>
        <CreateForm />
      </Card>
    </StyledCard>
  );
};

export default CreateTask;

const StyledCard = styled.div`
  padding: 16px;
`;
