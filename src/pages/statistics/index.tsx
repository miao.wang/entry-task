import { Card } from "antd";
import { observer } from "mobx-react";
import { FC } from "react";
import styled from "styled-components";
import store from "../../stores";
import { Line, LineConfig } from "@ant-design/charts";

interface Props {}

const Statistics: FC<Props> = () => {
  const { chartData } = store;
  const config: LineConfig = {
    data: chartData,
    padding: "auto",
    xField: "deadline",
    yField: "count",
    xAxis: {
      tickCount: 5,
    },
  };

  return (
    <StyledCard>
      <Card>
        <Line {...config} />
      </Card>
    </StyledCard>
  );
};

export default observer(Statistics);

const StyledCard = styled.div`
  padding: 16px;
`;
