import styled from "styled-components";
import Filter from "../../components/Filter";
import { message, Space, Table, Tag, Typography, Card } from "antd";
import { FC, useState } from "react";
import { observer } from "mobx-react";
import store from "../../stores";
import { TaskData, FilterInfo } from "../../types";
import dayjs from "dayjs";
import { ColumnType } from "antd/lib/table";
import { FilterValue } from "antd/lib/table/interface";

const { Link } = Typography;

interface Props {}

const TaskList: FC<Props> = () => {
  const { taskList, deleteTaskItem, finishTask } = store;
  const [filterInfo, setFilterInfo] = useState<FilterInfo>({
    content: [],
    priority: [],
    before: [],
    status: [],
  });

  const handleOnChange = (
    _: any,
    filters: Record<string, FilterValue | null>
  ) => {
    setFilterInfo({ ...filterInfo, status: filters.status as number[] });
  };

  const columns: ColumnType<TaskData>[] = [
    {
      title: "Priority",
      key: "priority",
      dataIndex: "priority",
      render: (priority: number) => <>{"P" + priority}</>,
      sorter: (a: TaskData, b: TaskData) => a.priority - b.priority,
      filteredValue: filterInfo.priority,
      onFilter: (v: any, r: TaskData) =>
        v[0] === "3" ? true : v.includes(r.priority),
    },
    {
      title: "Deadline",
      key: "deadline",
      dataIndex: "deadline",
      render: (deadline: number) => (
        <>{dayjs.unix(deadline).format("YYYY-MM-DD")}</>
      ),
      sorter: (a: TaskData, b: TaskData) => a.deadline - b.deadline,
      filteredValue: filterInfo.before,
      onFilter: (v: any, r: TaskData) =>
        v === "null" ? true : r.deadline <= dayjs(v).endOf("day").unix(),
    },
    {
      title: "Content",
      key: "content",
      dataIndex: "content",
      filteredValue: filterInfo.content,
      onFilter: (v: any, r: TaskData) => r.content.includes(v),
    },
    {
      title: "Status",
      key: "status",
      dataIndex: "status",
      render: (status: number) =>
        status === 1 ? (
          <Tag color="blue">doing</Tag>
        ) : status === 2 ? (
          <Tag color="green">done</Tag>
        ) : (
          <Tag color="red">failed</Tag>
        ),
      filters: [
        { text: "doing", value: 1 },
        { text: "done", value: 2 },
        { text: "failed", value: 3 },
      ],
      filteredValue: filterInfo.status,
      onFilter: (value: any, record: TaskData) => record.status === value,
    },
    {
      title: "Action",
      key: "action",
      render: (record: TaskData) => {
        return (
          <Space size="middle">
            <Link
              onClick={() => {
                try {
                  finishTask(record);
                } catch (e: any) {
                  message.error(e.message ?? "Failed");
                }
              }}
              disabled={record.status !== 1}
            >
              done
            </Link>
            <Link onClick={() => deleteTaskItem(record)}>delete</Link>
          </Space>
        );
      },
    },
  ];

  return (
    <StyledCard>
      <Card>
        <Filter setFilterInfo={setFilterInfo} />
        <Table
          columns={columns}
          dataSource={taskList}
          pagination={false}
          onChange={handleOnChange}
        />
      </Card>
    </StyledCard>
  );
};

export default observer(TaskList);

const StyledCard = styled.div`
  padding: 16px;
`;
