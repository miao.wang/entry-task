import { makeAutoObservable } from "mobx";
import { TaskData } from "../types";
import dayjs from "dayjs";

class Store {
  constructor() {
    makeAutoObservable(this);
    this.taskList = JSON.parse(localStorage.getItem("task") ?? "[]").map(
      (task: TaskData) =>
        task.deadline < dayjs().endOf("day").unix() && task.status === 1
          ? { ...task, status: 3 }
          : task
    );
  }

  taskList: TaskData[];

  addTaskItem = (postData: TaskData) => {
    if (this.taskList.some((task) => task.content === postData.content)) {
      throw new Error("Task already Existed!");
    }
    this.taskList.push(postData);
    localStorage.setItem("task", JSON.stringify(this.taskList));
  };
  deleteTaskItem = (task: TaskData) => {
    this.taskList = this.taskList.filter((item) => task !== item);
    localStorage.setItem("task", JSON.stringify(this.taskList));
  };
  finishTask = (task: TaskData) => {
    if (task.deadline < dayjs().unix()) {
      this.taskList = this.taskList.map((item) =>
        item === task ? { ...item, status: 3 } : item
      );
      localStorage.setItem("task", JSON.stringify(this.taskList));
      throw new Error("Already Failed");
    }
    this.taskList = this.taskList.map((item) =>
      item === task ? { ...item, status: 2 } : item
    );
    localStorage.setItem("task", JSON.stringify(this.taskList));
  };

  get chartData() {
    const map = new Map();
    this.taskList.forEach((task) => {
      const { deadline } = task;
      map.set(deadline, map.has(deadline) ? map.get(deadline) + 1 : 1);
    });
    return [...map]
      .sort((a, b) => a[0] - b[0])
      .map((item) => ({
        deadline: dayjs.unix(item[0]).format("YYYY-MM-DD"),
        count: item[1],
      }));
  }
}

export default new Store();
