import React, { FC } from "react";
import {
  EditOutlined,
  OrderedListOutlined,
  LineChartOutlined,
} from "@ant-design/icons";

interface route {
  path: string;
  main: React.LazyExoticComponent<FC>;
  menuName: string;
  icon: FC;
}

const CreateTask = React.lazy(() => import("./pages/create-task"));
const TaskList = React.lazy(() => import("./pages/task-list"));
const Statistics = React.lazy(() => import("./pages/statistics"));

const routes: route[] = [
  {
    path: "/create",
    main: CreateTask,
    menuName: "create task",
    icon: EditOutlined,
  },
  {
    path: "/list",
    main: TaskList,
    menuName: "task list",
    icon: OrderedListOutlined,
  },
  {
    path: "/statistics",
    main: Statistics,
    menuName: "statistics",
    icon: LineChartOutlined,
  },
];

export default routes;
