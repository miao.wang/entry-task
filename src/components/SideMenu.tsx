import { Menu } from "antd";
import { FC } from "react";
import routes from "../config";
import { useRouteMenu } from "../utils/hooks";

interface Props {}

const SideMenu: FC<Props> = () => {
  const { selectedKeys, chooseMenu } = useRouteMenu();

  return (
    <>
      <div className="icon" />
      <Menu
        mode="inline"
        theme="dark"
        selectedKeys={selectedKeys}
        onClick={chooseMenu}
      >
        {routes.map((route) => (
          <Menu.Item key={route.path} icon={<route.icon />}>
            {route.menuName}
          </Menu.Item>
        ))}
      </Menu>
    </>
  );
};

export default SideMenu;
