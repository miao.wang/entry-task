import { Select } from "antd";
import { RawValueType } from "rc-select/lib/Select";
import { FC, useEffect, useState } from "react";

const { Option } = Select;

interface Props {
  value?: number[];
  onChange?: (v: number[]) => void;
}

const PrioritySelector: FC<Props> = ({ onChange }) => {
  const [priorities, setProrities] = useState<number[]>([3]);

  const handleSelectPriorities = (e: RawValueType | any) => {
    if (e === 3) {
      setProrities([3]);
      onChange?.([3]);
    } else {
      const newValue = [...priorities, e].filter((item) => item !== 3);
      setProrities(newValue);
      onChange?.(newValue);
    }
  };
  const handleDeselect = (e: RawValueType | any) => {
    const newValue = priorities.filter((item) => item !== e);
    setProrities(newValue);
    onChange?.(newValue);
  };

  useEffect(() => {
    onChange?.([3]);
  }, []);

  return (
    <Select
      mode="multiple"
      value={priorities}
      onSelect={handleSelectPriorities}
      onDeselect={handleDeselect}
    >
      <Option value={3}>ALL</Option>
      <Option value={0}>P0</Option>
      <Option value={1}>P1</Option>
      <Option value={2}>P2</Option>
    </Select>
  );
};

export default PrioritySelector;
