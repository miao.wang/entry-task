import { Col, Form, Input, Row } from "antd";
import { FieldData } from "rc-field-form/lib/interface";
import { FC } from "react";
import DatePicker from "./DatePicker";
import PrioritySelector from "./PrioritySelector";
import { FilterInfo } from "../types";

interface Props {
  setFilterInfo: React.Dispatch<FilterInfo | ((F: FilterInfo) => FilterInfo)>;
}

const Filter: FC<Props> = ({ setFilterInfo }) => {
  const handleOnChange = ([{ name, value }]: FieldData[]) => {
    setFilterInfo((f) => ({ ...f, [(name as string[])[0]]: [value] }));
  };

  return (
    <Form onFieldsChange={handleOnChange}>
      <Row>
        <Col span={4}>
          <Form.Item
            name="content"
            label="Content"
            labelCol={{ span: 7, offset: 1 }}
            wrapperCol={{ span: 16 }}
          >
            <Input />
          </Form.Item>
        </Col>
        <Col span={3}>
          <Form.Item
            name="priority"
            label="Priority"
            labelCol={{ span: 10, offset: 1 }}
            wrapperCol={{ span: 13 }}
          >
            <PrioritySelector />
          </Form.Item>
        </Col>
        <Col span={4}>
          <Form.Item
            name="before"
            label="Before"
            labelCol={{ span: 5, offset: 1 }}
            wrapperCol={{ span: 18 }}
          >
            <DatePicker />
          </Form.Item>
        </Col>
      </Row>
    </Form>
  );
};

export default Filter;
