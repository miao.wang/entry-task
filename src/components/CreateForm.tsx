import { Button, Form, Input, message, Select } from "antd";
import DatePicker from "./DatePicker";
import { FC } from "react";
import store from "../stores";
import dayjs, { Dayjs } from "dayjs";
import { useForm } from "antd/es/form/Form";

const { Option } = Select;

interface Props {}

const CreateForm: FC<Props> = () => {
  const [form] = useForm();
  const { addTaskItem } = store;

  const formatTaskData = (formData: {
    content: string;
    deadline: Dayjs;
    priority: number;
  }) => {
    const deadline = formData.deadline.endOf("day").unix();
    return { ...formData, deadline, status: 1 };
  };
  const handleCreate = async () => {
    const formData = await form.validateFields();
    try {
      addTaskItem(formatTaskData(formData));
      message.success("create success");
    } catch (e: any) {
      message.error(e.message ?? "Failed");
    }
    form.resetFields();
  };

  return (
    <Form
      labelAlign="right"
      labelCol={{ span: 4 }}
      wrapperCol={{ span: 14 }}
      form={form}
    >
      <Form.Item
        label="Content"
        name="content"
        rules={[{ required: true, message: "Please input the task!" }]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Deadline"
        name="deadline"
        rules={[{ required: true, message: "Please select time!" }]}
      >
        <DatePicker
          disabledDate={(curr) => curr && curr < dayjs().startOf("day")}
        />
      </Form.Item>
      <Form.Item
        label="Priority"
        name="priority"
        rules={[{ required: true, message: "Please select the priority!" }]}
      >
        <Select>
          <Option value={0}>P0</Option>
          <Option value={1}>P1</Option>
          <Option value={2}>P2</Option>
        </Select>
      </Form.Item>
      <Form.Item label=" " colon={false}>
        <Button onClick={handleCreate} type="primary">
          Create
        </Button>
      </Form.Item>
    </Form>
  );
};

export default CreateForm;
